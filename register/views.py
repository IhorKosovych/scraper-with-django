# views.py
from django.shortcuts import render, redirect
from .forms import RegisterForm


# Create your views here.
def register(response):
    
    if response.method == "POST":
        form = RegisterForm(response.POST)
        print('here1')
        if form.is_valid():
            print('here')
            form.save()

            return redirect("/login")
    else:
        form = RegisterForm()
        print('here0')

        return render(response, "register.html", {"form":form})